sudo docker rm -f javacontainer
sudo mvn clean install
sudo chmod -R 777 target
sudo docker build -t mysql-img -f mysql-dockerfile .
sudo docker run --name mysqlcontainer -d -p 3306:3306 mysql-img
sudo docker build -t java-img -f java-dockerfile .
sudo docker run --name javacontainer -d -p 4040:9090 --link mysqlcontainer java-img

